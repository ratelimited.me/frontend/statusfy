module.exports = {
  // The basics
  title: 'RATELIMITED Status',
  name: 'ratelimited_status',
  description: 'Status page for RATELIMITED services',
  baseUrl: 'https://statusfy.ratelimited.me',
  
  // Notification-related settings
  notifications: {
      webpush: {
          onesignal: {
              appId: '0acf6876-9cbb-43bc-8d35-e24bbbae1067'
          }
      },
      twitter: {
          en: 'RLMEStatus'
      },
      support: {
          en: 'https://support.ratelimited.me'
      }
  },
 
  // Content-related settings
  content: {
      dir: 'content',
      frontMatterFormat: 'yaml',
      systems: [
           'frontend',
           'dashboard',
           'api',
           'raw-cdn',
           'cb-cdn',
           'raw-dl',
           'cb-dl'
      ]
  },
 
  // Theme-related settings
  theme: {
      links: {
          en: {
              contact: 'https://support.ratelimited.me',
              support: 'https://support.ratelimited.me',
              home: 'https://ratelimited.me',
              privacy: 'https://ratelimited.me/privacy'
          }
      }
  },
 
  // i18n-related settings
  locales: [
      {
          code: 'en',
          iso: 'en-US',
          name: 'English'
      }
  ]
}
